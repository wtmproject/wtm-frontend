import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { APP_ROUTER_PROVIDERS } from './app.routes';
import { AppRoutingModule } from './app-routing.module';

import {BrowserXhr, HttpModule, ResponseOptions, XHRBackend} from '@angular/http';
import {HttpInterceptor} from './service/http-interceptor';
import {ToastService} from './service/toast-service';
import {NotificationsService} from 'angular2-notifications';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoanModule} from './modules/loan/loan.module';
import { MainComponent } from './main/main.component';
import { AppComponent } from './app.component';
import {UserModule} from './modules/user/user.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';




@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    UserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
    LoanModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  declarations: [
    AppComponent,
    MainComponent
  ],
  providers: [HttpInterceptor,
    ToastService,
    NotificationsService

    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
