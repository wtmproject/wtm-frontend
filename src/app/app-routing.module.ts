import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from './modules/user/user-list/user-list.component';
import { Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {AppComponent} from './app.component';
import {LoanListComponent} from './modules/loan/loan-list/loan-list.component';
import {MainComponent} from './main/main.component';
import { LoanEditComponent } from './modules/loan/loan-edit/loan-edit.component';

const routes: Routes = [
  // {path: '', pathMatch: 'full', redirectTo: '/modules/user' },
  // {path: 'modules/user', component: UserListComponent}

  {
    path: '',
    component: MainComponent,
    children: [
      {path: '', component: UserListComponent  },
      {path: 'modules/user', component: UserListComponent},
      {path: 'modules/loan/:id', component: LoanListComponent},
      {path: 'modules/loan/:id/loan-edit/:id',  component: LoanEditComponent   },
    ]
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
