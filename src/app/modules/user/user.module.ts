import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list/user-list.component';
import {UserService} from '../../service/user.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserEditComponent } from './user-edit/user-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [UserListComponent, UserEditComponent],
  providers: [UserService]
})
export class UserModule { }
