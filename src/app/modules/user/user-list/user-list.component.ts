import {FormBuilder, FormGroup, Validators, NgForm, FormControl} from '@angular/forms';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormUser, UserModel} from '../../../model/user.model';
import {UserService} from '../../../service/user.service';
import {ToastService} from '../../../service/toast-service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [UserService]
})
export class UserListComponent implements OnInit {
  user: UserModel = new UserModel();
  formUser: FormUser = new FormUser()

  constructor( private route: ActivatedRoute,
               private router: Router,
               private formBuilder: FormBuilder,
               private service: UserService,
               private location: Location,
               private toast: ToastrService) { }

  ngOnInit() {

  }


  public search() {
    this.service.clearParameter();
    this.service.getUserEmail(this.formUser.email).subscribe(
      (response) => {
        this.user = response;
        if (this.user) {
          this.router.navigate(['/modules/loan/' + this.user.id], {relativeTo: this.route});
        }

      },
      ex => {
      });
  }

  public onSubmit() {
    this.service.save(this.user).subscribe(
      ( response ) => {
        this.user = response;
        this.toast.success('Sucesso!', 'Operação realizada com sucesso.');
      },
      ex => {
        this.toast.error('Atenção!', ex )

      });


  }
}
