import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoanModel} from '../../../model/loan.model';
import {LoanService} from '../../../service/loan.service';


@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.scss'],
  providers: [LoanService]
})
export class LoanListComponent implements OnInit {

  loan: LoanModel[] = [];
  idLoan: number;

  constructor(private service: LoanService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getLoanList();
  }

  public  getLoanList(): void {
    this.route.params.subscribe((value) => {
      this.idLoan = value['id'];
        this.retrieveList(value['id']);
      });
  }

  public retrieveList(id: number): void {
    this.service.getLoan(id).subscribe(
      (returnlist) => {
        this.loan = returnlist;
      },
      ex => {

      });
  }


  public goEdit() {

   this.router.navigate(['/modules/loan-edit/create'], {relativeTo: this.route});
 
    //this.router.navigate(['modules/loan/loan-edit/create']);
  }
}

