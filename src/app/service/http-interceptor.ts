
import { Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
// operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import {ToastService} from './toast-service';

import {Router} from '@angular/router';
import {TranslateService} from '../../translate/translate.service';

@Injectable()
export class HttpInterceptor extends Http {

  constructor(
    backend: XHRBackend,
    options: RequestOptions, private toast: ToastService,
    public http: Http,
    private router: Router
  ) {
    super(backend, options);
  }

  public request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options)
      .catch(this.handleError);
  }

  public handleError = (error: any) => {
    if (error.status === 0 ) {
      return Observable.empty();
    } else if (error.status === 404 ) {
      return Observable.empty();
    } if (error.status === 409 ) {
      return Observable.empty();
    } else if (error.status === 401 ) {
      return Observable.empty();
    } else if (error.status === 412 ) {
      return Observable.empty();
    } else {
      return Observable.throw(error);
    }
  }

}
