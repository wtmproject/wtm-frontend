import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {HttpInterceptor} from './http-interceptor';
import {UserModel} from '../model/user.model';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {LoanModel} from '../model/loan.model';




@Injectable()
export class LoanService extends BaseService<LoanModel> {

  constructor(protected http: HttpInterceptor) {
    super(http, '/rest/loan/');
  }

  public getLoan(user_id: any): Observable<LoanModel[]> {
    this.clearParameter();
    this.addParameter('userId', user_id);
    return this.http.get(`${this.fullUrl}loansByDebtor/`, this.addOptions(this.parameters)).map(
      response => response.json() as LoanModel
    ).catch(
      ex => Observable.throw(ex)
    );
  }


}
