import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {HttpInterceptor} from './http-interceptor';
import {UserModel} from '../model/user.model';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';




@Injectable()
export class UserService extends BaseService<UserModel> {

  constructor(protected http: HttpInterceptor) {
    super(http, 'user/');
  }

  public save(user: UserModel): Observable<any> {
    this.clearParameter();
    return this.http.post(this.fullUrl + 'save/', user, this.addOptions(this.parameters)).map(
      response => response.json() as UserModel
    ).catch(
      ex => Observable.throw(ex)
    );
  }


  public getUserEmail(email: string): Observable<UserModel> {
    this.clearParameter();
    this.addParameter('email', email.toString());
    return this.http.get(`${this.fullUrl}userByEmail/`, this.addOptions(this.parameters)).map(
      response => response.json() as UserModel
    ).catch(
      ex => Observable.throw(ex)
    );
  }


}
