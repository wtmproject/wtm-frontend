import {ModelBase} from './model-base';

export class UserModel extends ModelBase {
  public userId: number;
  public email = '';
  public name = '';
}

export  class FormUser {
  public  name: string;
  public email: string;
}
