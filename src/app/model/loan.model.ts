import {ModelBase} from './model-base';
import {UserModel} from './user.model';

export class LoanModel extends ModelBase {
  public id: number;
  public date_loan: any;
  public comment: string;
  public status: number;
  public payment_type: number;
  public amount: any;
  public debtor_id: number;
  public collector_id: number;
  

}



