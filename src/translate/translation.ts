import {InjectionToken} from '@angular/core';

/*import translations*/
import {LANG_EN_NAME, LANG_EN_TRANS} from './lang-en';
import {LANG_PT_NAME, LANG_PT_TRANS} from './lang-pt';

/*translation token*/
export const TRANSLATIONS = new InjectionToken('translations');

/*all translations*/
const dictionary = {};

dictionary[LANG_EN_NAME] = LANG_EN_TRANS;
dictionary[LANG_PT_NAME] = LANG_PT_TRANS;


/* providers */
export const TRANSLATION_PROVIDERS = [
  {provide: TRANSLATIONS, useValue: dictionary}
];
