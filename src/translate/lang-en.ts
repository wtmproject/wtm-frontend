export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {
  'MENU': {
    'HELLO': 'hello {{value}}'
  },
  'USER': {
    'NAME': 'Name',
    'EMAIL': 'Email'
  },
};
